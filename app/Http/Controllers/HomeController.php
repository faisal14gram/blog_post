<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // here auth user use to view only  login user post
    public function index()
    {
        $user=Auth::user();
        $posts=Post::orderBy('id','desc')->paginate(3);
        $categories=Category::all();
        return view('home',compact('categories','posts','user'));
    }

  public function search(Request $request)
    {
        $search=$request['search'];
        $posts=Post::orderBy('id','desc')->where('title','like','%'.$search.'%')->get();
        $categories=Category::all();
        return view('search',compact('categories','posts'));
    }
}
