<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\User;
use Carbon\Carbon;
use Auth;

class PostController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request){
    	 // dd($request->all());
        
        $insert=Post::insert([
            'title'=>$_POST['title'],
            'description'=>$_POST['description'],
            'category_id'=>$_POST['category_id'],
            'user_id'=>Auth::id(),
            'created_at'=>Carbon::now()->toDateTimeString(),
        ]);

        if($insert){
            return redirect('home');
        }else{
            return redirect('home');
        }
    }
// showing single  category post
    public function category($id){
    	$category=Category::where('id',$id)->firstOrFail();
    	return view('category',compact('category'));
    }  
    // showing single User post
    public function user($id){
        $user=User::where('id',$id)->firstOrFail();
        return view('user',compact('user'));
    }
}
