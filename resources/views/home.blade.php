@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                  <h2>Add Post</h2>
                  <form method="post" action="{{url('post/store')}}">
                   @csrf
                    <label>Enter Post Title</label>
                    <input type="text" name="title" class="form-control"> <br>

                    <label>Enter Post Description</label>
                    <textarea name="description" class="form-control" rows="5"></textarea> <br>
                  <label>Category</label>
                  <select class="form-control" name="category_id">
                    @foreach($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>  
                      @endforeach 
                  </select >
                    <input type="submit" value="Publish" class="mt-2 btn btn-success">
                  </form>
                 

                </div>

            </div>
           <div class=" card card-body">
               
            <h2>All Post</h2>

            @foreach($posts as $post)
            <div class=" card card-body mt-2">
                <h3>{{$post->title}} in <mark>
                  <a href="{{url('category/'.$post->category_id)}}">{{$post->category->name}}</a>
                </mark>User Name <a href="{{url('user/'.$post->user_id)}}">{{$post->user->name}}</a></h3>
            <div>
                {{$post->description}}
            </div>
            </div>
            @endforeach

           </div>
           <div> {{ $posts->links() }}</div>
          
             <div class=" card card-body">
               
            <h2>User Post</h2>

            @foreach($user->posts as $post)
            <div class=" card card-body mt-2">
                <h3>{{$post->title}} in <mark>
                  <a href="{{url('category/'.$post->category_id)}}">{{$post->category->name}}</a>
                </mark> Posted By {{$post->user->name}}</h3>
            <div>
                {{$post->description}}
            </div>
            </div>
            @endforeach

           </div>

        </div>
    </div>
</div>
@endsection
