@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Singel User Post</div>
           <div class=" card card-body">
               
            <h2>User Name= {{$user->name}}</h2>
             @foreach($user->posts as $post)
            <div class=" card card-body mt-2">
                <h3>{{$post->title}} in <mark>{{$post->category->name}}</mark> posted by <mark>{{$post->user->name}}</mark></h3>
            <div>
                {{$post->description}}
            </div>
            </div>
            @endforeach

           </div>

        </div>
    </div>
</div>
@endsection
